Changelog
=========

1.1.1 (unreleased)
------------------

- Nothing changed yet.


1.1.0 (2023-10-08)
------------------

- Use robot and not pybot command anymore.
  [flohcim]


1.0.0 (2022-05-10)
------------------

- Python 3 support only.
  [flohcim, tiazma]


0.4.0 (2018-04-10)
------------------

- Add 'additional_sql' option config to om-tests.
  [flohcim]


0.3.1 (2018-01-10)
------------------

- Removed unnecessary jinja2 env options
  [tiazma]


0.3.0 (2018-01-10)
------------------

- Added om-logo, application logo generation
  [tiazma]


0.2.2 (2017-11-25)
------------------

- Remove the externals was breaking travis CI.
  [flohcim]

0.2.1 (2017-11-24)
------------------

- Filename of EXTERNALS.txt file parsed was missing.
  [flohcim]

0.2.0 (2017-11-24)
------------------

- Add om-svnexternals script.
  [flohcim]


0.1 (2017-11-23)
----------------

- Initial release as a python package.
  [flohcim, fmichon, jymadier, mbroquet, nhaye, NHaye, nmeucci, oc1n, softime,
  stimezouaght, tiazma]
