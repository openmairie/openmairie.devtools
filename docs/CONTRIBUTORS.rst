Contributors
============

* Florent Michon <flohcim@gmail.com> [flohcim][fmichon]
* Grégory Malvolti <gmalvolti@atreal.fr> [gmalvolti]
* Jean-Yves Madier de Champvermeil <> [jymadier]
* Matthias Broquet <mbroquet@atreal.fr> [mbroquet][tiazma]
* Nicolas Haye <> [nhaye][NHaye]
* Nicolas Meucci <> [nmeucci][oc1n]
* Sofien Timezouaght <> [softime][stimezouaght]

